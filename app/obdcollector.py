import obd
import time
import gpscollector

connection = None

def init():
    global connection
    connection = obd.OBD() # auto-connects to USB or RF port

def collect_data():
    response = connection.query(obd.commands.SPEED)
    if response.is_null():
        return None
    speed = int(response.value.magnitude)

    response = connection.query(obd.commands.RPM)
    if response.is_null():
        return None
    rpm = int(response.value.magnitude)

    return { 'spd': speed, 'rpm': rpm }
