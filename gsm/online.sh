#!/bin/bash
SAKIS_CMD=/root/gsm/sakis3g
DISPLAY_CMD="/root/gsm/display.py"
TOGGLE_GSM_CMD="/root/gsm/toggle-gsm.py"
has_ethernet="`cat /sys/class/net/eth0/carrier`"
has_wireless="cat /sys/class/net/wlan0/carrier"
has_gsm="cat /sys/class/net/ppp0/carrier"

if [ "$has_ethernet" == "1" ]
then
  if [ -f "/sys/class/net/ppp0/carrier" ] && [ "`$has_gsm`" == "1" ]
  then
    $SAKIS_CMD --sudo "disconnect" | $DISPLAY_CMD 1
    sleep 1
  fi
  IPADDRESS="`/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`"
  echo "$IPADDRESS Eth01" | $DISPLAY_CMD 2
  exit 0
fi

if [ -f "/sys/class/net/wlan0/carrier" ] && [ "`$has_wireless`" == "1" ]
then
  if [ -f "/sys/class/net/ppp0/carrier" ] && [ "`$has_gsm`" == "1" ]
  then
    $SAKIS_CMD --sudo "disconnect" | $DISPLAY_CMD 1
    sleep 1
  fi
  IPADDRESS="`/sbin/ifconfig wlan0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`"
  echo "$IPADDRESS Wifi" | $DISPLAY_CMD 2
  exit
else
  if [ -f "/sys/class/net/ppp0/carrier" ] && [ "`$has_gsm`" == "1" ]
  then
    exit
  else
    $TOGGLE_GSM_CMD | $DISPLAY_CMD 1
    $SAKIS_CMD --sudo "connect" "OTHER=CUSTOM_TTY" "APN=internet"  | $DISPLAY_CMD 1
    echo "nameserver 8.8.8.8" > /etc/resolv.conf
    /root/startup.sh
  fi
  IPADDRESS="`/sbin/ifconfig ppp0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}'`"
  echo "$IPADDRESS GPRS" | $DISPLAY_CMD 2
fi




