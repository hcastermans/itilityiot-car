cd /root/gsm

if [ "`docker ps -a -q`" == "" ]
then
     /usr/local/bin/docker-compose up -d
else 
     /usr/local/bin/docker-compose restart
fi
