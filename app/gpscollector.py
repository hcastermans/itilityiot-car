import serial

GPS_FILTER = ['LL','VTG']

def init():
    return
    # check gps

def collect_data():
    report = {}
    with serial.Serial('/dev/ttyUSB0', 9600, timeout=1) as ser:
        to_float = lambda x: float(x) if x <> '' else  None
        line = ser.readline()
        data = line.split(',')
        header = data[0]

        if header == 'LL':
            # E.g. VTG,,T,,M,0.037,N,0.069,K,D*2D
            report['lat']  = to_float(data[1]) 
            report['lat2'] = str(data[2])
            report['lon']  = to_float(data[3])
            report['lon2'] = str(data[4])

        elif header == 'VTG':
            # E.g. VTG,,T,,M,0.067,N,0.123,K,D*27
            report['tt']     = to_float(data[1])
            report['mt']     = to_float(data[3])
            report['gs_kts'] = to_float(data[5])
            report['gs_km']  = to_float(data[7])

        else:
            return None

    return report

