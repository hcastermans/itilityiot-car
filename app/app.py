import logging
from time import sleep
import obdcollector
import gpscollector
import sys
import azure_messagebus

PAUSE=2

def main():
    if len(sys.argv) != 2:
        print("start with obd or gps as argument")
        sys.exit(1)

    if sys.argv[1] == 'obd':
        obd_thread()

    if sys.argv[1] == 'gps':
        gps_thread()

def gps_thread():
    gpscollector.init()
    while True:
        data = gpscollector.collect_data()
        if data <> None:
            print data
            azure_messagebus.send_message('cardata', data)

def obd_thread():
    obdcollector.init()
    while True:
        data = obdcollector.collect_data()
        if data <> None:
            print data
            azure_messagebus.send_message('cardata', data)
        sleep(PAUSE)

main()
