from azure.servicebus import ServiceBusService, Message, Queue
import config
import json
import requests
import time


bus_service = ServiceBusService(
    service_namespace=config.service_namespace,
    shared_access_key_name=config.shared_access_key_name,
    shared_access_key_value=config.shared_access_key_value)


def send_message_http(tag, payload):
    headers =  {'content-type': 'application/json'}
    endpoint = 'http://localhost:24224/%s' %  tag
    r = requests.post(endpoint, data=json.dumps(payload), headers=headers) # % json.dumps(payload))
    if r.status_code != 200:
        print r.status_code
        raise Exception('Could not deliver message %s to %s ' % (payload, endpoint))



def send_message(queue, payload):
     payload['t'] = int(time.time())
     bus_service.send_queue_message(queue, Message(json.dumps(payload)))
