#!/usr/bin/env python


# Python app to run a Cozir Sensor


import serial
import time
import signal
import sys
import pigpio


def handler(signum,frame):
    print 'You pressed CTRL+C! Exiting...'
    sys.exit(0)
    pi.bb_serial_read_close(RXD)
    pi.wave_delete(wid)
    pi.stop()
    sys.exit(0)


TXD=17

RXD=27


pi = pigpio.pi() # Connect to local host.

print "pigpio Connected!"


pi.bb_serial_read_open(RXD, 9600)

pi.set_mode(TXD,pigpio.OUTPUT)
pi.wave_clear()

#Set Cozir mode to polling
pi.wave_add_serial(TXD,9600,b"K 2\r\n")
wid = pi.wave_create()
pi.wave_send_once(wid)
time.sleep(1)



#signal handles for keyboard interrupt

signal.signal(signal.SIGINT, handler)


while True:

  try:
    data,b = pi.bb_serial_read(RXD)
    print b 

  except Exception,e:
    print "Error: %s"%(e)
    time.sleep(1)
    continue
